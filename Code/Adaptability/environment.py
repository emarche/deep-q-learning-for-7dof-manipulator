import numpy as np
import math
import random

class Environment:
    def __init__(self):
        #self.minRange = [-170, -105, -170, -180, -170, -5, -170]
        #self.maxRange = [170, 105, 170, 5, 170, 219, 170]
        self.minRange = [-50, -10, -50, -100, -50, 0, -50]
        self.maxRange = [50, 90, 50, 0, 50, 100, 50]

    def reset(self):
        pos = [0, 40, 0, -40, 0, 0, 0]
        self.counter = 0
        self.state = np.array(pos)

        t1 = np.random.randint(-50, 50)
        t2 = np.random.randint(-10, 90)
        t3 = np.random.randint(-50, 50)
        t4 = np.random.randint(-100, 0)
        t5 = np.random.randint(-50, 50)
        t6 = np.random.randint(-0, 100)
        t7 = np.random.randint(-50, 50)

        t1 -= (t1 % 5)
        t2 -= (t2 % 5)
        t3 -= (t3 % 5)
        t4 -= (t4 % 5)
        t5 -= (t5 % 5)
        t6 -= (t6 % 5)
        t7 -= (t7 % 5)

        target_angle = [t1, t2, t3, t4, t5, t6, t7]

        x, y, z = self.endEffectorPos( target_angle )
        self.target = [x, y, z]

        return np.concatenate((self.normalizeState(), np.array(self.target)))

    def step(self, action):
        step = 5
        timeout = 800
        error = 0.08
        done = False
        self.counter += 1

        if ( (action%2) == 0 and self.state[int(action/2)] < self.maxRange[int(action/2)] ):
            self.state[int(action/2)] += step

        if ( (action%2) == 1 and self.state[int(action/2)] > self.minRange[int(action/2)] ):
            self.state[int(action/2)] -= step

        x, y, z = self.endEffectorPos( self.state )
        #x, y, z = self.endEffectorPos( np.concatenate((self.state, [0, 0, 0])) )
        distance = math.sqrt( math.pow((x-self.target[0]), 2) + math.pow((y-self.target[1]), 2) + math.pow((z-self.target[2]), 2) )
        
        #reward = math.pow(math.e, -distance) - 1

        reward = 0

        if(self.counter >= timeout):
            reward = -1
            done = True

        if(distance <= error):
            reward = 1
            done = True

        return np.concatenate((self.normalizeState(), np.array(self.target))), reward, done, {}, self.counter, distance

    def normalizeState(self):
        normalizedState = []
        i = 0
        for deg in (self.state):
            normalizedState.append(deg / 100.0)
        return np.array(normalizedState)

    def endEffectorPos(self, joints):
        t1 = joints[0]
        t2 = joints[1]
        t3 = joints[2]
        t4 = joints[3]
        t5 = joints[4]
        t6 = joints[5]
        t7 = joints[6]

        x = (33*math.cos((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)) - math.cos((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)))/400 - (11*math.sin((math.pi*t7)/180)*(math.sin((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)) - math.cos((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)) - math.cos((math.pi*t5)/180)*(math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180) + math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.sin((math.pi*t3)/180))))/125 + (79*math.cos((math.pi*t1)/180)*math.sin((math.pi*t2)/180))/250 + (11*math.cos((math.pi*t7)/180)*(math.sin((math.pi*t6)/180)*(math.sin((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)) + math.cos((math.pi*t1)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180)) - math.cos((math.pi*t6)/180)*(math.cos((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)) - math.cos((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)) + math.sin((math.pi*t5)/180)*(math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180) + math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.sin((math.pi*t3)/180)))))/125 - (33*math.cos((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)))/400 + (48*math.sin((math.pi*t4)/180)*(math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180) - math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)))/125 + (33*math.sin((math.pi*t5)/180)*(math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180) + math.cos((math.pi*t1)/180)*math.cos((math.pi*t2)/180)*math.sin((math.pi*t3)/180)))/400 + (48*math.cos((math.pi*t1)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180))/125 + (33*math.cos((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180))/400
        y = (11*math.sin((math.pi*t7)/180)*(math.sin((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)) + math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)) - math.cos((math.pi*t5)/180)*(math.cos((math.pi*t1)/180)*math.cos((math.pi*t3)/180) - math.cos((math.pi*t2)/180)*math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180))))/125 - (33*math.cos((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)) + math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)))/400 + (79*math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180))/250 + (33*math.cos((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)))/400 - (48*math.sin((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)))/125 - (33*math.sin((math.pi*t5)/180)*(math.cos((math.pi*t1)/180)*math.cos((math.pi*t3)/180) - math.cos((math.pi*t2)/180)*math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180)))/400 - (11*math.cos((math.pi*t7)/180)*(math.sin((math.pi*t6)/180)*(math.sin((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)) - math.cos((math.pi*t4)/180)*math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180)) - math.cos((math.pi*t6)/180)*(math.cos((math.pi*t5)/180)*(math.cos((math.pi*t4)/180)*(math.cos((math.pi*t1)/180)*math.sin((math.pi*t3)/180) + math.cos((math.pi*t2)/180)*math.cos((math.pi*t3)/180)*math.sin((math.pi*t1)/180)) + math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180)) + math.sin((math.pi*t5)/180)*(math.cos((math.pi*t1)/180)*math.cos((math.pi*t3)/180) - math.cos((math.pi*t2)/180)*math.sin((math.pi*t1)/180)*math.sin((math.pi*t3)/180)))))/125 + (48*math.cos((math.pi*t4)/180)*math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180))/125 + (33*math.sin((math.pi*t1)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180))/400
        z = (79*math.cos((math.pi*t2)/180))/250 + (11*math.sin((math.pi*t7)/180)*(math.sin((math.pi*t5)/180)*(math.cos((math.pi*t2)/180)*math.sin((math.pi*t4)/180) - math.cos((math.pi*t3)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180)) - math.cos((math.pi*t5)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t3)/180)))/125 + (11*math.cos((math.pi*t7)/180)*(math.cos((math.pi*t6)/180)*(math.cos((math.pi*t5)/180)*(math.cos((math.pi*t2)/180)*math.sin((math.pi*t4)/180) - math.cos((math.pi*t3)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180)) + math.sin((math.pi*t2)/180)*math.sin((math.pi*t3)/180)*math.sin((math.pi*t5)/180)) + math.sin((math.pi*t6)/180)*(math.cos((math.pi*t2)/180)*math.cos((math.pi*t4)/180) + math.cos((math.pi*t3)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180))))/125 + (48*math.cos((math.pi*t2)/180)*math.cos((math.pi*t4)/180))/125 + (33*math.cos((math.pi*t2)/180)*math.sin((math.pi*t4)/180))/400 - (33*math.cos((math.pi*t5)/180)*(math.cos((math.pi*t2)/180)*math.sin((math.pi*t4)/180) - math.cos((math.pi*t3)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180)))/400 - (33*math.cos((math.pi*t3)/180)*math.cos((math.pi*t4)/180)*math.sin((math.pi*t2)/180))/400 + (48*math.cos((math.pi*t3)/180)*math.sin((math.pi*t2)/180)*math.sin((math.pi*t4)/180))/125 - (33*math.sin((math.pi*t2)/180)*math.sin((math.pi*t3)/180)*math.sin((math.pi*t5)/180))/400 + 0.333

        return x, y, z
