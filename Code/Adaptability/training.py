# 0) Remove warning and base import
from __future__ import print_function
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import matplotlib.pyplot as plt
from collections import deque
import random
import math

# 1) Import Numnpy
import numpy as np
from numpy import loadtxt

# 2) Import Keras
import keras
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Activation, InputLayer, Dropout

# 3) Import Environment
from environment import Environment
env = Environment()
env.reset()

# 4) Setting up variables
out_node = 14

num_episodes = 200000
y = 0.95
eps = 1.0
eps_min = 0.01
decay_factor = 0.999

r_list = []
moves_list = []
success_list = []
memory = deque(maxlen=10000)

memory_win = deque (maxlen = 1000)
memory_lose = deque (maxlen = 1000)

success_queue = deque(maxlen = 100)

# 5) Setting up the ANN
model = Sequential()
model.add(Dense(50, input_shape = (10, ), activation='tanh'))
model.add(Dense(50, activation = 'tanh'))
model.add(Dense(50, activation = 'tanh'))
model.add(Dense(out_node, activation = 'linear'))
model.compile(loss='mse', optimizer=keras.optimizers.Adam(lr=0.001), metrics=['mae'])


if os.path.isfile("backup.h5"):
    model.load_weights("backup.h5")
    eps = eps_min


# 6) Learning Function
def learn():
    state_list = []
    target_list = []
    sample_batch = []

    batch_size = 24
    batch_size_win = 24
    batch_size_lose = 24

    if(len(memory) < batch_size):
        batch_size = len(memory)

    if(len(memory_win) < batch_size_win):
        batch_size_win = len(memory_win)

    if(len(memory_lose) < batch_size_lose):
        batch_size_lose = len(memory_lose)

        
    sample_batch.append(random.sample(memory, batch_size))
    sample_batch.append(random.sample(memory_win, batch_size_win))
    sample_batch.append(random.sample(memory_lose, batch_size_lose))


    for i in range(3):
        for state, action, reward, next_state, done in sample_batch[i]:
            target = reward
            if not done:
                target = reward + y * np.amax(model.predict(np.array([next_state])))
            target_f = model.predict(np.array([state]))[0]
            target_f[action] = target
            state_list.append(state)
            target_list.append(target_f)
    
    model.fit(np.array(state_list), np.array(target_list), epochs=1, verbose=0)

# 7) Execute the Q-Learning
try:
    for i in range(num_episodes):
        s = env.reset()
        done = False
        while not done:
            if np.random.random() < eps:
                a = np.random.randint(0, out_node)
            else:
                a = np.argmax(model.predict(np.array([s])))
            new_s, r, done, _, moves, distance = env.step(a)

            if(r == 1):
                memory_win.append([s, a, r, new_s, done])
            elif(r == -1):
                memory_lose.append([s, a, r, new_s, done])
            else:
                memory.append([s, a, r, new_s, done])

            s = new_s
        learn()
        if(eps > eps_min):
            eps *= decay_factor
        
        moves_list.append(moves)
        r_list.append(r)
        success_queue.append(r)

        success = int(success_queue.count(1)/(len(success_queue)+0.0)*100)
        success_list.append(success)
        
        print("Generation " + str(i) + ", Reward: " + str(r) + ", Success: " + str(success) + ", " + str(eps))

        if(success > 90):
            model.save("backup_" + str(success) + "_" + str(i) + ".h5")


finally:
    print("")
    '''model.save("backup.h5")
    np.savetxt("success.txt", success_list, fmt='%3f')
    np.savetxt("move.txt", moves_list, fmt='%3f')
    np.savetxt("reward.txt", r_list, fmt='%3f')
    plt.plot(success_list)
    plt.show()

model.save("backup.h5")
np.savetxt("success.txt", success_list, fmt='%3f')
np.savetxt("move.txt", moves_list, fmt='%3f')
np.savetxt("reward.txt", r_list, fmt='%3f')
plt.plot(success_list)
plt.show()'''
